
## Tickr Carbon Offsetting Challenge

Simply run `sh start.sh` and everything is ready to go

### Approach

Have kept it as simple as possible, have added the endpoint `/carbon-offset-schedule`, used a service 
`SubscriptionScheduleService` to do the processing which I injected in the `SubscriptionController` 
controller via an interface (contract in Laravel) `SubscriptionScheduleServiceContract` - which in turn 
is set by the `AppServiceProvider` through the `boot()` method

Have added tests for all the cases presented in the brief, and they all pass `vendor/bin/phpunit`

### Observation 

Given that the instructions were to KISS (Keep It Simple Stupid), I reverted from my original approach 
of using a database, adding Laravel Sanctum for authentication (of a mobile client), then using the Bearer 
token to access the endpoint `/carbon-offset-schedule` to simply using no database and testing all the functionality. 
I can of course expand at the next stage (hopefully)
