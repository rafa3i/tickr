<?php

namespace Database\Factories;

use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::factory()->create();
        $length = rand(1, 36);
        return [
            'user_id' => $user->id,
            'subscription_start' => now(),
            'subscription_end' => now()->addMonthsNoOverflow($length),
            'length_months' => $length,
        ];
    }
}
