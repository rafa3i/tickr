<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Tests\TestCase;

class PaymentScheduleTest extends TestCase
{
    /** @test */
    public function request_without_params_returns_http_bad_request()
    {
        $this->json('GET', 'api/carbon-offset-schedule')
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function request_with_only_start_date_returns_http_bad_request()
    {
        $params = [
            'subscriptionStartDate' => now(),
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function request_with_only_length_returns_http_bad_request()
    {
        $params = [
            'scheduleInMonths' => 1,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function request_with_past_start_date_and_length_of_five_returns_http_ok_with_correct_data()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2021', '01', '07')->format('Y-m-d'),
            'scheduleInMonths' => 5,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    "2021-02-07",
                    "2021-03-07",
                    "2021-04-07",
                    "2021-05-07",
                    "2021-06-07"
                ]
            );
    }

    /** @test */
    public function request_with_old_start_date_and_length_of_two_returns_http_ok_with_correct_data()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('1998', '01', '07')->format('Y-m-d'),
            'scheduleInMonths' => 2,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    "1998-02-07",
                    "1998-03-07"
                ]
            );
    }

    /** @test */
    public function request_with_close_end_of_month_start_date_and_length_of_three_returns_http_ok_with_correct_data()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2020', '01', '30')->format('Y-m-d'),
            'scheduleInMonths' => 3,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    "2020-02-29",
                    "2020-03-30",
                    "2020-04-30"
                ]
            );
    }

    /** @test */
    public function request_with_end_of_month_start_date_and_length_of_one_returns_http_ok_with_empty_json()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2020', '01', '31')->format('Y-m-d'),
            'scheduleInMonths' => 1,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                "2020-02-29"
            ]);
    }

    /** @test */
    public function request_with_past_start_date_and_length_of_zero_returns_http_ok_with_empty_json()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2020', '01', '10')->format('Y-m-d'),
            'scheduleInMonths' => 0,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson([]);
    }

    /** @test */
    public function request_with_future_start_date_and_length_of_two_returns_http_bad_request_with_error()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2031', '01', '30')->format('Y-m-d'),
            'scheduleInMonths' => 2,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertJson([
                "error" => [
                    "Subscription cannot start in the future"
                ]
            ])
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /** @test */
    public function request_with_past_start_date_and_length_of_two_hundred_returns_http_bad_request_with_error()
    {
        $params = [
            'subscriptionStartDate' => Carbon::create('2020', '01', '30')->format('Y-m-d'),
            'scheduleInMonths' => 200,
        ];

        $this->json('GET', 'api/carbon-offset-schedule', $params)
            ->assertJson([
                "error" => [
                    "The maximum available subscription is 36 months"
                ]
            ])
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
