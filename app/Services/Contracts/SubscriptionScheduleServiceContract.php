<?php

namespace App\Services\Contracts;

use Illuminate\Http\Request;

interface SubscriptionScheduleServiceContract
{
    public function paymentSchedule(Request $request);
}
