<?php

namespace App\Services;

use App\Services\Contracts\SubscriptionScheduleServiceContract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class SubscriptionScheduleService implements SubscriptionScheduleServiceContract
{
    /**
     * @param Request $request
     * @return array|Response
     */
    public function paymentSchedule(Request $request)
    {
        $validator = $this->validate($request);

        if($validator->fails()) {
            return response([
                'error' => $validator->errors()->all()
            ], 400);
        }

        $subscriptionStart = Carbon::create(request()->subscriptionStartDate);
        $subscriptionLength = request()->scheduleInMonths;

        $subscriptionPaymentDates = [];
        for($i = 1; $i <= $subscriptionLength; $i++) {
            // Can use ->toIso8601String() for a full ISO8601 representation, this is only for the requested Y-m-d
            $subscriptionPaymentDates[] = $subscriptionStart->copy()->addMonthsNoOverflow($i)->format('Y-m-d');
        }

        return $subscriptionPaymentDates;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validate(Request $request): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($request->all(), [
            'subscriptionStartDate' => 'required|date_format:Y-m-d|before_or_equal:'.now(),
            'scheduleInMonths' => 'required|int|max:36',
        ],
            [
                'subscriptionStartDate.before_or_equal' => 'Subscription cannot start in the future',
                'scheduleInMonths.max' => 'The maximum available subscription is 36 months'
            ]);
    }
}
