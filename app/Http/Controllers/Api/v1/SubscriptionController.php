<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\Contracts\SubscriptionScheduleServiceContract;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    private SubscriptionScheduleServiceContract $subscriptionScheduleService;

    /**
     * SubscriptionController constructor.
     * @param SubscriptionScheduleServiceContract $subscriptionScheduleService
     */
    public function __construct(SubscriptionScheduleServiceContract $subscriptionScheduleService)
    {
        $this->subscriptionScheduleService = $subscriptionScheduleService;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function paymentSchedule(Request $request)
    {
        return $this->subscriptionScheduleService->paymentSchedule($request);
    }
}
